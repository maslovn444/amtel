# Use the official Python image
FROM python:3.9

# Set the working directory in the container
WORKDIR /app

# Copy the dependencies file to the working directory
COPY requirements.txt .

# Install dependencies
RUN pip install -r requirements.txt

COPY . .

# Expose the port uvicorn runs on
EXPOSE 8000

# Command to run the uvicorn server for your application
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
