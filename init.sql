-- Create the database with the specified namec

DO $$
BEGIN
    IF NOT EXISTS (SELECT FROM pg_database WHERE datname = 'postgres_db') THEN
        CREATE DATABASE "postgres_db";
    END IF;
END $$;

-- Connect to the created database
\c postgres_db;

-- Create a table with the specified user, password, and columns
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL
);

-- Insert sample data into the users table using the specified values
INSERT INTO users (name, password) VALUES ('mns',  'postgres_db');
