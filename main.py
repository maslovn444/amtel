
from fastapi import FastAPI, File, UploadFile
from fastapi.responses import FileResponse
from pydantic import BaseModel
import os
import uuid
from datetime import datetime
import psycopg2
import time

app = FastAPI()

def wait_for_postgres():
    max_attempts = 10
    attempts = 0
    connected = False

    while attempts < max_attempts and not connected:
        try:
            conn = psycopg2.connect(
                dbname="postgres_db",
                user="mns",
                password="postgres_db",
                host="postgres",
                port="5432"
            )
            connected = True
            print("Successfully connected to PostgreSQL!")
            return conn  # Возвращаем объект соединения
        except psycopg2.OperationalError as e:
            print(f"Connection to PostgreSQL failed. Retrying... ({attempts+1}/{max_attempts})")
            attempts += 1
            time.sleep(5)  # Wait for 5 seconds before retrying

    if not connected:
        print("Failed to connect to PostgreSQL after multiple attempts. Exiting...")
        exit()  # Выход из приложения, если подключение не удалось

# Пытаемся подключиться к PostgreSQL
conn = wait_for_postgres()
cur = conn.cursor()

class FileData(BaseModel):
    uid: str
    filename: str
    date: str

@app.get("/v1/find")
def find_files(filename: str = None, date: str = None, UUID: str = None):
    # Implement logic to find files based on filename, date, and UUID
    return [{"uid": "UID", "filename": "filename", "date": "date"}]

@app.post("/v2/upload")
async def upload_file(file: UploadFile = File(...)):
    uid = str(uuid.uuid4())
    filename = file.filename
    upload_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    
    # Save file to disk
    with open(f"uploaded_files/{uid}", "wb") as f:
        f.write(await file.read())
    
    # Insert file data into PostgreSQL
    cur.execute("INSERT INTO files (uid, filename, upload_date) VALUES (%s, %s, %s)", (uid, filename, upload_date))
    conn.commit()
    
    return {"UUID": uid}

@app.get("/v1/download")
def download_file(UUID: str):
    # Implement logic to download file based on UUID
    return FileResponse(f"uploaded_files/{UUID}")
